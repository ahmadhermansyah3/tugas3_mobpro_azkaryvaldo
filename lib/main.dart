import 'package:flutter/material.dart';
import 'get_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'fect api',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(
          title: 'Tugas Mobprog',
        ));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  UserGet userGet = null;

  @override
  void initState() {
    super.initState();
    UserGet.connectToApiUser('6').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(30.0),
            ),
            Text(
              "Welcome",
              style: TextStyle(
                  color: Color(0xff0563BA),
                  fontSize: 48,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
            ),
            Container(
                width: 128,
                height: 128,
                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: new NetworkImage((userGet != null) ? userGet.avatar : "https://reqres.in/img/faces/6-image.jpg")))),
            Text(
              (userGet != null)
                  ? userGet.firstName + " " + userGet.lastName
                  : "Tidak ada data user",
              style: TextStyle(
                  color: Color(0xFF0563BA),
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              (userGet != null) ? userGet.email : "Tidak ada data",
            )
          ],
        ),
      ),
    );
  }
}
