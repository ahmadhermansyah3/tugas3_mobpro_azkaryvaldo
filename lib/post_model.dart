import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class PostResult{
  String id;
  String name;
  String job;
  String created;

  //buat konstruktor nya
  PostResult({
    this.id, this.name, this.job, this.created
  });

  //buat factory metode, membuat object PostResult
  //object nya hasil mapping dari JSON Object

  factory PostResult.createPostResult(Map<String, dynamic> object)
  {
    return PostResult(
      id: object['id'],
      name: object['name'],
      job: object['job'],
      created: object['createdAt']
       );
  }
}